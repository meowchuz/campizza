<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registrations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('gender');
            $table->string('date_of_birth');
            $table->integer('schedule');
            $table->integer('user');
            $table->string('current_school');
            $table->string('grade_in_the_fall');
            $table->string('tshirt_size');
            $table->integer('number_of_tshirt')->default(1);

            $table->string('doctor_name');
            $table->string('insurance');
            $table->string('doctor_phone');
            $table->string('policy_holder');
            $table->string('illnesses')->nullable();
            $table->string('allergies')->nullable();
            $table->string('medication');
            $table->string('medication_name')->nullable();
            $table->string('activities');
            $table->string('activities_name')->nullable();
            $table->string('medical_treatments');
            $table->string('medical_treatments_name')->nullable();
            $table->string('immunizations');
            $table->string('tetanus');
            $table->string('comments')->nullable();


 
            $table->boolean('Week1AM')->default(false);
            $table->boolean('Week1PM')->default(false);

            $table->boolean('Week2AM')->default(false);
            $table->boolean('Week2PM')->default(false);

            $table->boolean('Week3AM')->default(false);
            $table->boolean('Week3PM')->default(false);

            $table->boolean('Week4AM')->default(false);
            $table->boolean('Week4PM')->default(false);

            $table->boolean('Week5AM')->default(false);
            $table->boolean('Week5PM')->default(false);

            $table->boolean('Week6AM')->default(false);
            $table->boolean('Week6PM')->default(false);
            
            $table->boolean('extended_care')->default(false);
            $table->integer('session_price')->default(0);
            $table->integer('extended_care_price')->default(0);
            $table->integer('total_price')->default(0);

            $table->boolean('active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('registrations');
    }
}
