<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_owner = Role::where('name', 'owner')->first();

        $owner = new User();
        $owner->email = 'info@campizza.com';
        $owner->password = bcrypt('4Campizza');
        $owner->fullFill = true;
        $owner->save();
        $owner->roles()->attach($role_owner);
    }
}
