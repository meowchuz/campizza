/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 49);
/******/ })
/************************************************************************/
/******/ ({

/***/ 49:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(50);


/***/ }),

/***/ 50:
/***/ (function(module, exports) {

var sessionPriceItems = document.getElementsByClassName('session-price');
var displayTotalItem = document.getElementById('display-total');
var sessionPriceItem = document.getElementById('session-price');
var extendedCarePriceItem = document.getElementById('extended-care-price');
var totalPriceItem = document.getElementById('total-price');
var extendedCareItem = document.getElementById('extended-care');

_.each(sessionPriceItems, function (sessionPriceItem) {
  sessionPriceItem.addEventListener('change', checkout);
});

extendedCareItem.addEventListener('change', checkout);

function checkout() {
  var week1 = document.querySelector('input[name="week_1"]:checked').dataset.price;
  var week2 = document.querySelector('input[name="week_2"]:checked').dataset.price;
  var week3 = document.querySelector('input[name="week_3"]:checked').dataset.price;
  var week4 = document.querySelector('input[name="week_4"]:checked').dataset.price;
  var week5 = document.querySelector('input[name="week_5"]:checked').dataset.price;
  var isIncludeExtendedCare = extendedCareItem.checked;

  var numberOfWeeks = 0;
  var priceWeek1 = parseInt(week1);
  var priceWeek2 = parseInt(week2);
  var priceWeek3 = parseInt(week3);
  var priceWeek4 = parseInt(week4);
  var priceWeek5 = parseInt(week5);

  if (0 !== priceWeek1) {
    numberOfWeeks++;
  }

  if (0 !== priceWeek2) {
    numberOfWeeks++;
  }

  if (0 !== priceWeek3) {
    numberOfWeeks++;
  }

  if (0 !== priceWeek4) {
    numberOfWeeks++;
  }

  if (0 !== priceWeek5) {
    numberOfWeeks++;
  }

  var sessionTotal = priceWeek1 + priceWeek2 + priceWeek3 + priceWeek4 + priceWeek5;
  var extendedCareTotal = isIncludeExtendedCare ? 15 * 5 * numberOfWeeks : 0;
  var total = sessionTotal + extendedCareTotal;

  displayTotalItem.innerText = total;
  sessionPriceItem.value = sessionTotal;
  extendedCarePriceItem.value = extendedCareTotal;
  totalPriceItem.value = total;
}

/***/ })

/******/ });