# Camp Izza

## Requirements
- PHP >= 5.5.9
- Laravel 5.2
- Composer

## Configuration
Copy .env file into project from [Wiki](https://github.com/meow-at-vincent/CampIzza/wiki/Configuration)

## Development
Run migration
```
php artisan migrate
```

Open Terminal #1
```
npm run watch
```

Open Terminal #2
```
php artisan serve
```

## Mock data
Generate fake user information
https://www.fakenamegenerator.com/

Generate fake credit card
https://developer.paypal.com/developer/creditCardGenerator/
