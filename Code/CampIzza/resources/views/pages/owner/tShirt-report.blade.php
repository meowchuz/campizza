@extends('layouts.dashboard')

@section('content')

<div class="card">
    <form action="{{ route('tShirt-report') }}" method="POST">
        {{ csrf_field() }}
        <div class="card-header">
            <h3 class="allergies">T-Shirt Report</h3>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col">
                    &nbsp;
                </div>
                <div class="col" align="right">
                    <label for="export-CSV">Export to CSV file</label>
                    <button type="submit" class="btn btn-primary">Export</button>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    &nbsp;
                </div>
                <div class="col">
                    <!-- Search form -->
                    <div class="active-pink-3 active-pink-4 mb-4">
                        <input class="form-control" type="text" placeholder="Search" auto aria-label="Search">
                    </div>
                </div>

            </div>
        </div>

        <!-- Report Table -->
        <div class="card-footer">
            <table class="table table-sm">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">First Name</th>
                        <th scope="col">Last Name</th>
                        <th scope="col">Gender</th>
                        <th scope="col">Grade</th>
                        <th scope="col">Shirt Size</th>
                        <th scope="col"># of Shirts</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($extendedData as $data)
                    <tr>
                        <td>{{ $data->id }}</td>
                        <td>{{ $data->first_name }}</td>
                        <td>{{ $data->last_name }}</td>
                        <td>{{ $data->gender }}</td>
                        <td>{{ $data->grade_in_the_fall }}</td>
                        <td>{{ $data->tshirt_size }}</td>
                        <td>{{ $data->number_of_tshirt }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </form>
</div>

@endsection