@extends('layouts.dashboard')

@section('content')

<div class="card">
    <form action="{{ route('extendedCare-report') }}" method="POST">
        {{ csrf_field() }}
        <div class="card-header">
            <h3 class="extendCare">Extended-Care Report</h3>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col">
                    &nbsp;
                </div>
                <div class="col" align="right">
                    <label for="export-CSV">Export to CSV file</label>
                    <button type="submit" class="btn btn-primary">Export</button>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label for="extend-filter">Sort by</label>
                    <select name="extended-sort">
                        <option value="new" selected>Newest</option>
                        <option value="name">Name</option>
                    </select>
                </div>
                <div class="col">
                    <!-- Search form -->
                    <div class="active-pink-3 active-pink-4 mb-4">
                        <input class="form-control" type="text" placeholder="Search" auto aria-label="Search">
                    </div>
                </div>

            </div>
        </div>

        <!-- Report Table -->
        <div class="card-footer">
            <table class="table table-sm">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">First Name</th>
                        <th scope="col">Last Name</th>
                        <th scope="col">Gender</th>
                        <th scope="col">Grade</th>
                        <th scope="col">Extended Care</th>
                        <th scope="col">ExtendedCare Price Total</th>
                        <th scope="col">Week1AM</th>
                        <th scope="col">Week1PM</th>
                        <th scope="col">Week2AM</th>
                        <th scope="col">Week2PM</th>
                        <th scope="col">Week3AM</th>
                        <th scope="col">Week3PM</th>
                        <th scope="col">Week4AM</th>
                        <th scope="col">Week4PM</th>
                        <th scope="col">Week5AM</th>
                        <th scope="col">Week5PM</th>
                        <th scope="col">Week6AM</th>
                        <th scope="col">Week6PM</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($extendedData as $data)
                    <tr>
                        <td>{{ $data->id }}</td>
                        <td>{{ $data->first_name }}</td>
                        <td>{{ $data->last_name }}</td>
                        <td>{{ $data->gender }}</td>
                        <td>{{ $data->grade_in_the_fall }}</td>
                        <td>{{ $data->extended_care }}</td>
                        <td>{{ $data->extended_care_price }}</td>
                        <td>{{ $data->Week1AM }}</td>
                        <td>{{ $data->Week1PM }}</td>
                        <td>{{ $data->Week2AM }}</td>
                        <td>{{ $data->Week2PM }}</td>
                        <td>{{ $data->Week3AM }}</td>
                        <td>{{ $data->Week3PM }}</td>
                        <td>{{ $data->Week4AM }}</td>
                        <td>{{ $data->Week4PM }}</td>
                        <td>{{ $data->Week5AM }}</td>
                        <td>{{ $data->Week5PM }}</td>
                        <td>{{ $data->Week6AM }}</td>
                        <td>{{ $data->Week6PM }}</td>
                    </tr>
                @endforeach   
                </tbody>
            </table>
            <div class="col">
            <h2>{{ $extendedData->links() }}</h2>
            </div>
        </div>
    </form>
</div>

@endsection