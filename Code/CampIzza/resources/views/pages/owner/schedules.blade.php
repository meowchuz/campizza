@extends('layouts.dashboard') 
@section('pageTitle') Schedules
@endsection
 
@section('header')
<div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center bg--green">
    &nbsp;
</div>
@endsection
 
@section('scripts') {{--
<script src="{{ asset('js/pages/owner/campers.js') }}"></script> --}}
@endsection
 
@section('content')
<div class="container mt--7">
    <div class="card shadow">
        <div class="card-header bg-white border-0">
            <div class="d-flex align-items-center justify-content-between">
                <h2 class="main-title mb-0">List of Schedules</h2>
                <button class="btn btn-blue" data-toggle="modal" data-target="#addModal">
                    <i class="fa fa-calendar mr-2" aria-hidden="true"></i> Add schedule
                </button>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table align-items-center table-flush">
                <thead class="thead-light">
                    <th>Start date</th>
                    <th>End date</th>
                    <th>Notice</th>
                    <th>&nbsp;</th>
                </thead>
                <tbody>
                    @if (sizeof($schedules))
                        @foreach ($schedules as $schedule)
                            <tr>
                                <td>
                                    {{ $schedule->start_date }}
                                </td>
                                <td>
                                    {{ $schedule->end_date }}
                                </td>
                                <td>
                                    {{ $schedule->notice }}
                                </td>
                                <td class="text-right">
                                    <button class="btn btn-white no-shadow btn-parent" type="button" data-schedule="{{ $schedule->id }}" data-toggle="modal" data-target="#scheduleModal">
                                        <i class="fa fa-calendar mr-2" aria-hidden="true" data-schedule="{{ $schedule->id }}"></i> Details
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="4" class="text-center text-gray">
                                No schedules
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
        <div class="card-footer py-4">
            <nav aria-label="pagination">
                <ul class="pagination justify-content-end mb-0">
                    @if ($schedules->previousPageUrl())
                    <li class="page-item">
                        <a class="page-link" href="{{ $schedules->previousPageUrl() }}" tabindex="-1">
                    @else
                    <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1">
                    @endif
                            <i class="fa fa-angle-left"></i>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>
                    <li class="page-item d-flex align-items-center mx-4">
                        {{ $start }} to {{ $end }} of {{ $schedules->total() }}
                    </li>
                    @if ($schedules->nextPageUrl())
                    <li class="page-item">
                        <a class="page-link" href="{{ $schedules->nextPageUrl() }}">
                    @else
                    <li class="page-item disabled">
                        <a class="page-link" href="#">
                    @endif
                            <i class="fa fa-angle-right"></i>
                            <span class="sr-only">Next</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>

<!-- Add schedule modal -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="{{ route('addSchedule') }}" method="post">
                <div class="modal-header">
                    <h5 class="modal-title text-primary" id="addModalLabel">Add schedule</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-control-label" for="input-state-date">Start date</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                                    </div>
                                    <input id="input-state-date" class="form-control datepicker" name="start_date" placeholder="Select date" type="text" value="06/20/2018">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-control-label" for="input-end-date">End date</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                                    </div>
                                    <input id="input-end-date" class="form-control datepicker" name="end_date" placeholder="Select date" type="text" value="06/20/2018">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label" for="input-notice">Notice</label>
                        <input type="text" id="input-notice" class="form-control form-control-alternative" 
                            placeholder="Notice" autocomplete="off" spellcheck="false" name="notice">
                    </div>
                </div>
                <div class="modal-footer">
                    {{ csrf_field() }}
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Add schedule</button>
                </div>
        </div>
        </form>
    </div>
</div>
@endsection