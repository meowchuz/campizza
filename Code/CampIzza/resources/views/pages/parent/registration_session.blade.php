@extends('layouts.dashboard')

@section('pageTitle')
Register camp
@endsection

@section('header')
<div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center bg--green">
    &nbsp;
</div>
@endsection

@section('scripts')
<script src="{{ asset('js/pages/parent/registration_session.js') }}"></script>
@endsection

@section('content')
<div class="container mt--7">
    <form action="{{ route('handleRegisterCampAboutSession', ['camper' => $camper->id]) }}" method="post">
        <div class="card shadow">
            <div class="card-header bg-white border-0">
                <h2 class="main-title mb-0">Registration form</h2>
                <p>
                    Camper: <strong>{{ $camper->first_name }} {{ $camper->last_name }}</strong>
                </p>
            </div>
            <div class="card-body">
                <h3 class="secondary-title mb-4">Session information</h3>
                <div class="row">
                    <div class="col-md-8" id="sessions">
                        @php
                            $index = 1
                        @endphp
                        @foreach ($schedule['schedule'] as $week => $timeline)
                            <div class="form-group">
                                <label for="{{ $week }}_BOTH" class="d-block">
                                    <input class="mr-2 session-price" id="{{ $week }}_BOTH" type="radio" name="{{ $week }}" value="{{ $week }}_BOTH" data-price="245"> <strong>Week {{ $index++ }}</strong> ({{ $timeline }}) - Both Session: $245
                                </label>
                                <label for="{{ $week }}_AM" class="d-block">
                                    <input class="mr-2 session-price" id="{{ $week }}_AM" type="radio" name="{{ $week }}" value="{{ $week }}_AM" data-price="145"> Morning Session (8:30 AM - 12:00 PM): $145
                                </label>
                                <label for="{{ $week }}_PM" class="d-block">
                                    <input class="mr-2 session-price" id="{{ $week }}_PM" type="radio" name="{{ $week }}" value="{{ $week }}_PM" data-price="145"> Afternoon Session (12:30 AM - 4:00 PM): $145
                                </label>
                                <label for="{{ $week }}_NONE" class="d-block">
                                    <input class="mr-2 session-price" id="{{ $week }}_NONE" type="radio" name="{{ $week }}" value="{{ $week }}_NONE" data-price="0" checked> None
                                </label>
                            </div>
                        @endforeach
                    </div>
                </div>
                <h3 class="secondary-title">Extended care</h3>
                <label for="extended-care">
                    <input id="extended-care" type="checkbox" name="extended_care" value="1" class="mr-2"> I would like to buy extended care from <strong>7:00 AM - 8:30 AM</strong> and/or <strong>4:00PM - 5:30PM</strong> at $15/day
                </label>
                <hr>
                <h3 class="secondary-title">Total: $<span id="display-total">0</span></h3>
                <input type="hidden" name="session_price" id="session-price">
                <input type="hidden" name="extended_care_price" id="extended-care-price">
                <input type="hidden" name="total_price" id="total-price">
            </div>
            <div class="card-footer bg-white border-0 text-right">
                {{ csrf_field() }}
                <button class="btn btn-secondary" type="button" id="btn-back">Back</button>
                <button class="btn btn-brown" type="submit" id="btn-submit">Next</button>
            </div>
        </div>
    </form>
</div>
@endsection
