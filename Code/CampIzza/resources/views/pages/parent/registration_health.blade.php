@extends('layouts.dashboard') 
@section('pageTitle') Register camp
@endsection
 
@section('header')
<div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center bg--green">
    &nbsp;
</div>
@endsection

@section('content')
<div class="container mt--7">
    <form action="{{ route('handleRegisterCampAboutHealth', ['camper' => $camper->id]) }}" method="post">
        <div class="card shadow">
            <div class="card-header bg-white border-0">
                <h2 class="main-title mb-0">Registration form</h2>
                <p>
                    Camper: <strong>{{ $camper->first_name }} {{ $camper->last_name }}</strong>
                </p>
            </div>
            <div class="card-body">
                <h3 class="secondary-title">Health</h3>
                <h4 class="sub-title">Doctor information</h4>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-doctor-name">Doctor name <i class="fa fa-asterisk text-danger" aria-hidden="true"></i></label>
                            <input type="text" id="input-doctor-name" class="form-control form-control-alternative"
                                @if (isset($health->doctor_name))
                                    value="{{ $health->doctor_name }}"
                                @endif
                                placeholder="Name" autocomplete="off" spellcheck="false" name="doctor_name" required>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-insurance">Insurance carrier <i class="fa fa-asterisk text-danger" aria-hidden="true"></i></label>
                            <input type="text" id="input-insurance" class="form-control form-control-alternative"
                                @if (isset($health->insurance))
                                    value="{{ $health->insurance }}"
                                @endif
                                placeholder="Insurance carrier" autocomplete="off" spellcheck="false" name="insurance" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-doctor-phone">Doctor's phone number <i class="fa fa-asterisk text-danger" aria-hidden="true"></i></label>
                            <input type="tel" id="input-doctor-phone" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" class="form-control form-control-alternative"
                                @if (isset($health->doctor_phone))
                                    value="{{ $health->doctor_phone }}"
                                @endif
                                placeholder="###-###-####" autocomplete="off" spellcheck="false" name="doctor_phone" required>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-policy-holder">Policy Holder's Name <i class="fa fa-asterisk text-danger" aria-hidden="true"></i></label>
                            <input type="text" id="input-policy-holder" class="form-control form-control-alternative"
                                @if (isset($health->policy_holder))
                                    value="{{ $health->policy_holder }}"
                                @endif
                                placeholder="Policy Holder's Name" autocomplete="off" spellcheck="false" name="policy_holder" required>
                        </div>
                    </div>
                </div>
                <h4 class="sub-title">Personal health status</h4>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-illnesses">Chronic conditions or illnesses of which we shoyuld be aware</label>
                            <input type="text" id="input-illnesses" class="form-control form-control-alternative" 
                                @if (isset($health->illnesses))
                                    value="{{ $health->illnesses }}"
                                @endif
                                placeholder="Chronic conditions or illnesses of which we shoyuld be aware" autocomplete="off" spellcheck="false" name="illnesses">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-allergies">Allergies and/or Dietary Restrictions</label>
                            <input type="text" id="input-allergies" class="form-control form-control-alternative"
                                @if (isset($health->allergies))
                                    value="{{ $health->allergies }}"
                                @endif
                                placeholder="Allergies and/or Dietary Restrictions" autocomplete="off" spellcheck="false" name="allergies">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-medication">Will your child be taking any medication at camp</label>
                            <div class="row">
                                <div class="col-4">
                                    <select name="medication" id="input-medication" class="form-control form-control-alternative">
                                        @foreach (['No', 'Yes'] as $answer)
                                            @if (isset($health->medication) && $health->medication == $answer)
                                                <option value="{{ $answer }}" selected>{{ $answer }}</option>
                                            @else
                                                <option value="{{ $answer }}">{{ $answer }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-8">
                                    <input type="text" id="input-medication-name" class="form-control form-control-alternative"
                                        @if (isset($health->medication_name))
                                            value="{{ $health->medication_name }}"
                                        @endif
                                        placeholder="If &quot;Yes&quot;, please list medications" autocomplete="off" spellcheck="false" name="medication_name">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-activities">Are there any activities at camp that your child cannot participate in</label>
                            <div class="row">
                                <div class="col-4">
                                    <select name="activities" id="input-activities" class="form-control form-control-alternative">
                                        @foreach (['No', 'Yes'] as $answer)
                                            @if (isset($health->activities) && $health->activities == $answer)
                                                <option value="{{ $answer }}" selected>{{ $answer }}</option>
                                            @else
                                                <option value="{{ $answer }}">{{ $answer }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-8">
                                    <input type="text" id="input-activities-name" class="form-control form-control-alternative" 
                                        @if (isset($health->activities_name))
                                            value="{{ $health->activities_name }}"
                                        @endif
                                        placeholder="Are there any activities at camp that your child cannot participate in" autocomplete="off" spellcheck="false" name="activities_name">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-medical-treatments">Has your child undergone any medical treatments</label>
                            <div class="row">
                                <div class="col-4">
                                    <select name="medical_treatments" id="input-medical-treatments" class="form-control form-control-alternative">
                                        @foreach (['No', 'Yes'] as $answer)
                                            @if (isset($health->medical_treatments) && $health->medical_treatments == $answer)
                                                <option value="{{ $answer }}" selected>{{ $answer }}</option>
                                            @else
                                                <option value="{{ $answer }}">{{ $answer }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-8">
                                    <input type="text" id="input-medical-treatments-name" class="form-control form-control-alternative" 
                                        @if (isset($health->medical_treatments_name))
                                            value="{{ $health->medical_treatments_name }}"
                                        @endif
                                        placeholder="If &quot;Yes&quot;, please list all medical treatments" autocomplete="off" spellcheck="false" name="medical_treatments_name">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-immunizations">Has your child received all current immunizations</label>
                            <select name="immunizations" id="input-immunizations" class="form-control form-control-alternative">
                                @foreach (['Yes', 'No'] as $answer)
                                    @if (isset($health->immunizations) && $health->immunizations == $answer)
                                        <option value="{{ $answer }}" selected>{{ $answer }}</option>
                                    @else
                                        <option value="{{ $answer }}">{{ $answer }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-tetanus">Date of last tetanus shot <i class="fa fa-asterisk text-danger" aria-hidden="true"></i></label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                                </div>
                                <input id="input-tetanus" class="form-control datepicker" name="tetanus" required
                                    @if (isset($health->tetanus))
                                        value="{{ $health->tetanus }}"
                                    @else
                                        value="{{ date('m/d/Y') }}"
                                    @endif
                                    placeholder="Select date" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-comments">Adition comments about this camper</label>
                            <input type="text" id="input-comments" class="form-control form-control-alternative"
                                @if (isset($health->comments))
                                    value="{{ $health->comments }}"
                                @endif
                                placeholder="Please write here" autocomplete="off" spellcheck="false" name="comments">
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer bg-white border-0">
                <div class="row">
                    <div class="col-md-6">
                        <i class="fa fa-asterisk text-danger" aria-hidden="true"></i> Indicates require fileds
                    </div>
                    <div class="col-md-6 text-right">
                        {{ csrf_field() }}
                        <button class="btn btn-secondary" type="button" id="btn-back">Back</button>
                        <button class="btn btn-brown" type="submit" id="btn-submit">Next</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection