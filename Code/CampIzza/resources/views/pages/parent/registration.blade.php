@extends('layouts.dashboard') 
@section('pageTitle') Register camp
@endsection
 
@section('header')
<div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center bg--green">
    &nbsp;
</div>
@endsection
 
@section('content')
<div class="container mt--7">
    <div class="card shadow">
        <div class="card-header bg-white border-0">
            <div class="d-flex align-items-center justify-content-between">
                <h3 class="main-title mb-0">Registration for Campers</h3>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table align-items-center table-flush">
                <thead class="thead-light">
                    <th>Camper's name</th>
                    <th>Gender</th>
                    <th class="text-right">Date of Birth</th>
                    <th></th>
                </thead>
                <tbody>
                    @if (sizeof($campers)) @foreach ($campers as $camper)
                    <tr>
                        <td>
                            {{ $camper->name }}
                        </td>
                        <td>
                            {{ ucfirst($camper->gender) }}
                        </td>
                        <td class="text-right">
                            {{ $camper->birthday }}
                        </td>
                        <td class="text-right">
                            <a href="{{ route('registerCampAboutInformation', ['camper' => $camper->id]) }}" class="btn btn-white no-shadow btn-registration"
                                role="button" data-camper="{{ $camper->id }}" id="camper-{{ $camper->id }}">
                                <i class="fa fa-sign-in" aria-hidden="true"></i> Register camp
                            </a>
                        </td>
                    </tr>
                    @endforeach @else
                    <tr>
                        <td colspan="4" class="text-center text-gray">
                            No campers
                        </td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection