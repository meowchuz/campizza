@extends('layouts.dashboard') 
@section('pageTitle') Register camp
@endsection
 
@section('header')
<div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center bg--green">
    &nbsp;
</div>
@endsection
 
@section('scripts')
<script src="https://www.paypalobjects.com/api/checkout.js"></script>
<script src="{{ asset('js/pages/parent/registration_review.js') }}"></script>
@endsection
 
@section('content')
<div class="container mt--7">
    <form id="registration-form" action="{{ route('handleRegisterCampAboutReview', ['camper' => $camper->id]) }}" method="post">
        <div class="card shadow">
            <div class="card-header bg-white border-0">
                <h2 class="main-title mb-0">Registration form</h2>
                <p>
                    Camper: <strong>{{ $camper->first_name }} {{ $camper->last_name }}</strong>
                </p>
            </div>
            <div class="card-body">
                <h3 class="secondary-title text-blue">Information</h3>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-current-school">Current school</label>
                            <input type="text" id="input-current-school" class="form-control form-control-alternative" disabled value="{{ $registrationForm->current_school }}">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-grade-in-the-fall">Grade in the Fall</label>
                            <input type="text" id="input-grade-in-the-fall" class="form-control form-control-alternative" disabled value="{{ $registrationForm->grade_in_the_fall }}">
                        </div>
                    </div>
                </div>
                <hr>
                <h3 class="secondary-title text-blue">Uniform</h3>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-tshirt-size">T-shirt size</label>
                            <input type="text" id="input-tshirt-size" class="form-control form-control-alternative" disabled value="{{ $registrationForm->tshirt_size }}">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-number-of-tshirt">Numbers of T-Shirt</label>
                            <input type="number" id="input-number-of-tshirt" class="form-control form-control-alternative" disabled value="{{ $registrationForm->number_of_tshirt }}">
                        </div>
                    </div>
                </div>
                <h3 class="secondary-title text-blue">Health</h3>
                <h4 class="heading-small text-muted">Doctor information</h4>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-doctor-name">Name</label>
                            <input type="text" id="input-doctor-name" class="form-control form-control-alternative" disabled value="{{ $registrationForm->doctor_name }}">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-insurance-carrier">Insurance carrier</label>
                            <input type="text" id="input-insurance-carrier" class="form-control form-control-alternative" disabled value="{{ $registrationForm->insurance }}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-doctor-phone-number">Phone number</label>
                            <input type="text" id="input-doctor-phone-number" class="form-control form-control-alternative" disabled value="{{ $registrationForm->doctor_phone }}">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-policy-holders-name">Policy Holder's Name</label>
                            <input type="text" id="input-policy-holders-name" class="form-control form-control-alternative" disabled value="{{ $registrationForm->policy_holder }}">
                        </div>
                    </div>
                </div>
                <h4 class="heading-small text-muted">Personal health status</h4>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-illnesses">Chronic conditions or illnesses of which we shoyuld be aware</label>
                            <input type="text" id="input-illnesses" class="form-control form-control-alternative" disabled value="{{ $registrationForm->illnesses }}">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-allergies">Allergies and/or Dietary Restrictions?</label>
                            <input type="text" id="input-allergies" class="form-control form-control-alternative" disabled value="{{ $registrationForm->allergies }}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-medication">Will your child be taking any medication at camp?</label>
                            <div class="row">
                                <div class="col-4">
                                    <select name="medication" id="input-medication" class="form-control form-control-alternative" disabled value="{{ $registrationForm->medication }}">
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </div>
                                <div class="col-8">
                                    <input type="text" id="input-medication-name" class="form-control form-control-alternative" disabled value="{{ $registrationForm->medication_name }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-activities">Are there any activities at camp that your child cannot participate in?</label>
                            <div class="row">
                                <div class="col-4">
                                    <select name="activities" id="input-activities" class="form-control form-control-alternative" disabled value="{{ $registrationForm->activities }}">
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </div>
                                <div class="col-8">
                                    <input type="text" id="input-activities-name" class="form-control form-control-alternative" disabled value="{{ $registrationForm->activities_name }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-medical-treatments">Has your child undergone any medical treatments?</label>
                            <div class="row">
                                <div class="col-4">
                                <select name="medical_treatments" id="input-medical-treatments" class="form-control form-control-alternative" disabled value="{{ $registrationForm->medical_treatments}}">
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </div>
                                <div class="col-8">
                                    <input type="text" id="input-medical-treatments-name" class="form-control form-control-alternative" disabled value="{{ $registrationForm->medical_treatments_name}}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-immunizations">Has your child received all current immunizations?</label>
                            <select name="Immunizations" id="input-immunizations" class="form-control form-control-alternative" disabled value="{{ $registrationForm->immunizations}}">
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-tetanus">Date of last tetanus shot</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                                </div>
                                <input id="input-tetanus" class="form-control datepicker" type="text" disabled value="{{ $registrationForm->tetanus}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-comments">Any comments about this camper?</label>
                            <input type="text" id="input-comments" class="form-control form-control-alternative" disabled value="{{ $registrationForm->comments}}">
                        </div>
                    </div>
                </div>
                <hr>
                <h3 class="secondary-title text-blue">Session</h3>
                <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">&nbsp;</th>
                                <th scope="col">Week 1</th>
                                <th scope="col">Week 2</th>
                                <th scope="col">Week 3</th>
                                <th scope="col">Week 4</th>
                                <th scope="col">Week 5</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr scope="row">
                                <td>Morning (8:30 AM - 12:00 PM)</td>
                                @for ($index = 1; $index < 6; $index++)
                                    <td>
                                        @if ($registrationForm->{'Week' . $index . 'AM'})
                                            <i class="fa fa-check text-green" aria-hidden="true"></i>
                                        @endif
                                    </td>
                                @endfor
                            </tr>
                            <tr scope="row">
                                <td>Afternoon (12:30 AM - 4:00 PM)</td>
                                @for ($index = 1; $index < 6; $index++)
                                    <td>
                                        @if ($registrationForm->{'Week' . $index . 'PM'})
                                            <i class="fa fa-check text-green" aria-hidden="true"></i>
                                        @endif
                                    </td>
                                @endfor
                            </tr>
                            <tr scope="row">
                                <td>Extended care</td>
                                @for ($index = 1; $index < 6; $index++)
                                    <td>
                                        @if ($registrationForm->extended_care)
                                            @if ($registrationForm->{'Week' . $index . 'AM'} || $registrationForm->{'Week' . $index . 'PM'})
                                                <i class="fa fa-check text-green" aria-hidden="true"></i>
                                            @endif
                                        @endif
                                    </td>
                                @endfor
                            </tr>
                        </tbody>
                    </table>
                </div>
                <hr>
                <h3 class="secondary-title text-blue">Total of Amount</h3>
                <div class="row">
                    <div class="col-md-5">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>Sessions</td>
                                    <td>${{ $registrationForm->session_price }}</td>
                                </tr>
                                <tr>
                                    <td>Extended care</td>
                                    <td>${{ $registrationForm->extended_care_price }}</td>
                                </tr>
                                <tr class="font-weight-bold">
                                    <td>Total</td>
                                    <td>${{ $registrationForm->total_price }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <hr>
                <h3 class="secondary-title text-blue">Terms and Conditions</h3>
                <p>
                    I am aware of the camp activities described on the camp website and I give my permission for my child to participate in these
                    activities, unless indicated above.
                </p>
                <p>
                    The information entered is true to the best of my knowledge. I understand that I am financially responsible for all fees
                    and that all payments must be received by the first day of camp. All fees are non-refundable and there
                    will be no refunds or exchanges for missed days. Parents are asked to notify Camp Izza if their child
                    is ill or will not be attending as expected. Camp Director will attempt to call parents/guardians and/or
                    emergency contacts if campers do not arrive to camp when expected.
                </p>
                <p>
                    I authorize Camp Izza to have and use the photos and video of the person named above in camp promotional materials. I agree
                    to release, hold harmless, and indemnify Camp Izza, its trustees, staff, family members of employees,
                    vendors, students, volunteers or insurers, or their heirs or representatives for any and all claims of
                    any nature whatsoever, including, but not limited to, those related to and arising from personal injuries,
                    illnesses, or fatality that my child may suffer or incur while he/she is on the Camp Izza campus or while
                    using the facilities and equipment. I agree to not hold Camp Izza responsible for loss of or damage to
                    any possessions my child brings to the camp and campus. I hereby agree to indemnify Camp Izza against
                    any claims of any third parties (including, but not exclusively, members of the child's family and other
                    camp participants) for damages or losses incurred by them as a result of a child's participation in Camp
                    Izza or presence on campus.
                </p>
                <p>
                    I understand that registration is on a first-come, first serve basis, that my camper's spot will only be reserved upon receipt
                    of payment and that returned checks will incur a $25 fee.
                </p>

                <input type="checkbox" name="checkbox" value="check" id="agree" /> I have read and agree to the Terms and Conditions

            </div>
            <div class="card-footer bg-white border-0 d-flex flex-row-reverse">
                <input type="hidden" id="total-price" value="{{ $registrationForm->total_price }}">
                {{ csrf_field() }}
                <div id="paypal-button-container"></div>
                <div class="position-relative">
                    <button class="btn btn-secondary mr-4 position-absolute right-0" type="button" id="btn-back">Back</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection