@extends('layouts.dashboard')

@section('pageTitle')
Register camp
@endsection

@section('header')
<div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center bg--green">
    &nbsp;
</div>
@endsection

@section('content')
<div class="container mt--7">
    <form action="{{ route('handleRegisterCampAboutInformation', ['camper' => $camper->id]) }}" method="post">
        <div class="card shadow">
            <div class="card-header bg-white border-0">
                <h2 class="main-title mb-0">Registration form</h2>
                <p>
                    Camper: <strong>{{ $camper->first_name }} {{ $camper->last_name }}</strong>
                </p>
            </div>
            <div class="card-body">
                <h3 class="secondary-title">Education</h3>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-current-school">Current school</label>
                            <input type="text" id="input-current-school" class="form-control form-control-alternative"
                                placeholder="Name of school" autocomplete="off" spellcheck="false" name="current_school">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-grade-in-the-fall">Grade in the Fall</label>
                            <input type="text" id="input-grade-in-the-fall" class="form-control form-control-alternative" 
                                placeholder="Please input here" autocomplete="off" spellcheck="false" name="grade_in_the_fall">
                        </div>
                    </div>
                </div>
                <h3 class="secondary-title">Camp shirt</h3>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-tshirt-size">T-shirt size</label>
                            <select name="tshirt_size" id="input-tshirt-size" class="form-control form-control-alternative">
                                @foreach ($tShirtSizes as $size)
                                    <option value="{{ $size }}">{{ $size }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="input-number-of-tshirt">Numbers of T-Shirt</label>
                            <select id="input-number-of-tshirt" class="form-control form-control-alternative" name="number_of_tshirt">
                                <option value="1" selected>1 T-Shirt: Free</option>
                                <option value="2">2 T-Shirts: $10</option>
                                <option value="3">3 T-Shirts: $20</option>
                                <option value="4">4 T-Shirts: $30</option>
                                <option value="5">5 T-Shirts: $40</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer bg-white border-0 text-right">
                {{ csrf_field() }}
                <button class="btn btn-brown" type="submit" id="btn-submit">Next</button>
            </div>
        </div>
    </form>
</div>
@endsection
