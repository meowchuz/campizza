// Render the PayPal button

const totalPrice = document.getElementById('total-price').value;
const registrationForm = document.getElementById('registration-form');

paypal.Button.render(
  {
    // Set your environment

    env: 'sandbox', // sandbox | production

    // Specify the style of the button

    style: {
      label: 'buynow',
      size: 'large', // small | medium | large | responsive
      shape: 'rect', // pill | rect
      color: 'gold', // gold | blue | silver | black
      fundingicons: true
    },

    // PayPal Client IDs - replace with your own
    // Create a PayPal app: https://developer.paypal.com/developer/applications/create

    client: {
      sandbox:
        'AYERdADsz8ROrg9vffFHY5dUy6hUtQObdlb2fk3KgxeEmwC97YIG2C9xQnHdR3invc_Ym7lWZ7SsKqLL',
      production: '<insert production client id>'
    },

    payment: function(data, actions) {
      return actions.payment.create({
        payment: {
          transactions: [
            {
              amount: { total: totalPrice, currency: 'USD' }
            }
          ]
        }
      });
    },

    onAuthorize: function(data, actions) {
      return actions.payment.execute().then(function() {
        registrationForm.submit();
      });
    }
  },
  '#paypal-button-container'
);
