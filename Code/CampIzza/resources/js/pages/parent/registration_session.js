let sessionPriceItems = document.getElementsByClassName('session-price');
const displayTotalItem = document.getElementById('display-total');
const sessionPriceItem = document.getElementById('session-price');
const extendedCarePriceItem = document.getElementById('extended-care-price');
const totalPriceItem = document.getElementById('total-price');
const extendedCareItem = document.getElementById('extended-care');

_.each(sessionPriceItems, sessionPriceItem => {
  sessionPriceItem.addEventListener('change', checkout);
});

extendedCareItem.addEventListener('change', checkout);

function checkout() {
  const week1 = document.querySelector('input[name="week_1"]:checked').dataset
    .price;
  const week2 = document.querySelector('input[name="week_2"]:checked').dataset
    .price;
  const week3 = document.querySelector('input[name="week_3"]:checked').dataset
    .price;
  const week4 = document.querySelector('input[name="week_4"]:checked').dataset
    .price;
  const week5 = document.querySelector('input[name="week_5"]:checked').dataset
    .price;
  const isIncludeExtendedCare = extendedCareItem.checked;

  let numberOfWeeks = 0;
  let numberOfSpecialWeeks = 0;
  const priceWeek1 = parseInt(week1);
  const priceWeek2 = parseInt(week2);
  const priceWeek3 = parseInt(week3);
  const priceWeek4 = parseInt(week4);
  const priceWeek5 = parseInt(week5);

  if (0 !== priceWeek1) {
    numberOfWeeks++;
  }

  if (0 !== priceWeek2) {
    numberOfSpecialWeeks++;
  }

  if (0 !== priceWeek3) {
    numberOfWeeks++;
  }

  if (0 !== priceWeek4) {
    numberOfWeeks++;
  }

  if (0 !== priceWeek5) {
    numberOfWeeks++;
  }

  const sessionTotal = priceWeek1 + priceWeek2 + priceWeek3 + priceWeek4 + priceWeek5;
  

  const extendedCareTotal = (isIncludeExtendedCare) ? ((15 * 5 * numberOfWeeks) + (15 * 4 * numberOfSpecialWeeks)) : 0;
  const total = sessionTotal + extendedCareTotal;

  displayTotalItem.innerText = total;
  sessionPriceItem.value = sessionTotal;
  extendedCarePriceItem.value = extendedCareTotal;
  totalPriceItem.value = total;
}
