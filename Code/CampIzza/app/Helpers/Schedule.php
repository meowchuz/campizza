<?php

namespace App\Helpers;

class Schedule
{
    /**
     * Get attributes of schedule
     * 
     * @return array
     */
    public function getScheduleAttributes() {
        $attributes = [
            'start_date',
            'end_date',
            'notice'
        ];

        return $attributes;
    }
}
