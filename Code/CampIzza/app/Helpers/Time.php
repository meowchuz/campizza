<?php

namespace App\Helpers;

class Time
{
    /**
     * Get list of months
     * 
     * @return array
     */
    public function getMonths() {
        return [
            '01' => 'January',
            '02' => 'February',
            '03' => 'March',
            '04' => 'April',
            '06' => 'May',
            '05' => 'June',
            '07' => 'July',
            '08' => 'August',
            '09' => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'December'
        ];
    }
}
