<?php

namespace App\Helpers;

class Camper
{
    /**
     * Get camper attributes for registration
     * 
     * @param string|null $mode
     * @return array
     */
    public function getCamperAttributes($mode = null) {
        $attributes = [
            'profile' => [
                'first_name',
                'last_name',
                'gender',
                'date_of_birth'
            ],
            'information' => [
                'current_school',
                'grade_in_the_fall',
                'tshirt_size',
                'number_of_tshirt'
            ],
            'health' => [
                'doctor_name',
                'insurance',
                'doctor_phone',
                'policy_holder',
                'illnesses',
                'allergies',
                'medication',
                'medication_name',
                'activities',
                'activities_name',
                'medical_treatments',
                'medical_treatments_name',
                'immunizations',
                'tetanus',
                'comments'
            ],
            'session' => [
                'week_1',
                'Week1AM',
                'Week1PM',
                'week_2',
                'Week2AM',
                'Week2PM',
                'week_3',
                'Week3AM',
                'Week3PM',
                'week_4',
                'Week4AM',
                'Week4PM',
                'week_5',
                'Week5AM',
                'Week5PM',
                'extended_care',
                'session_price',
                'extended_care_price',
                'total_price'
            ],
            'session_attributes_should_insert_to_database' => [
                'Week1AM',
                'Week1PM',
                'Week2AM',
                'Week2PM',
                'Week3AM',
                'Week3PM',
                'Week4AM',
                'Week4PM',
                'Week5AM',
                'Week5PM',
                'extended_care',
                'extended_care_price',
                'session_price',
                'total_price',
            ]
        ];

        if ('information' == $mode) {
            return $attributes['information'];
        } elseif ('health' == $mode) {
            return $attributes['health'];
        } elseif ('session' == $mode) {
            return $attributes['session'];
        } elseif ('profile' == $mode) {
            return $attributes['profile'];
        }
    
        return array_merge(
            $attributes['information'],
            $attributes['health'],
            $attributes['session_attributes_should_insert_to_database']
        );
    }

    /**
     * Get list of T-Shirt sizes
     * 
     * @return array
     */
    public function getTShirtSizes() {
        return [
            'S', 'M', 'L', 'XL'
        ];
    }

    /**
     * Get phone types
     * 
     * @return array
     */
    public function getPhoneTypes() {
        return [
            'cell' => 'Cell',
            'home' => 'Home',
            'work' => 'Work',
            'other' => 'Other'
        ];
    }
}
