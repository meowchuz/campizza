<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class Camper extends Facade{
    protected static function getFacadeAccessor() { 
        return 'Camper'; 
    }
}
