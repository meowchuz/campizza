<?php

namespace App\Repositories;

use DB;
use Carbon\Carbon;

class ScheduleRepository
{
    /**
     * Save schedule
     * 
     * @param object $schedule
     * @return void
     */
    public function saveSchedule($schedule) {
        $status = DB::table('schedules')->insert([
            'start_date' => $schedule->start_date,
            'end_date' => $schedule->end_date,
            'notice' => $schedule->notice,
            'week_1' => $schedule->week_1,
            'week_2' => $schedule->week_2,
            'week_3' => $schedule->week_3,
            'week_4' => $schedule->week_4,
            'week_5' => $schedule->week_5,

            'created_at' =>  Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }

    /**
     * Get all schedules with pagination
     * 
     * @return Illuminate\Support\Collection;
     */
    public function getSchedulesWithPagination() {
        $schedules = DB::table('schedules')->paginate(20);
        return $schedules;
    }
}
