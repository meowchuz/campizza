<?php

namespace App\Repositories;

use DB;
use Carbon\Carbon;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CamperRepository
{
    /**
     * Add camper aka child
     * 
     * @param object $camper
     * @return void
     */
    public function addCamper($camper) {
        DB::table('campers')->insert([
            'first_name' => $camper->first_name,
            'last_name' => $camper->last_name,
            'gender' => $camper->gender,
            'date_of_birth' => $camper->date_of_birth,

            'parent' => $camper->parent,
            'created_at' =>  Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }

    public function storeCamperInfo($camper) {
        DB::table('registrations')->insert([
            'first_name' => $camper->first_name,
            'last_name' => $camper->last_name,
            'gender' => $camper->gender,
            'date_of_birth' => $camper->date_of_birth,
            'user' => Auth::user()->id,

            'created_at' =>  Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }

    /**
     * Get all children of parentID
     * 
     * @param integer $parent
     * @return Illuminate\Support\Collection;
     */
    public function getCampers($parent) {
        $campers = DB::table('campers')->where('parent', $parent)->get();
        return $campers;
    }

    /**
     * Get all children
     * 
     * @return Illuminate\Support\Collection;
     */
    public function getCampersWithPagination() {
        $campers = DB::table('campers')->paginate(20);
        return $campers;
    }

    /**
     * Add camper aka child
     * 
     * @param object $camper
     * @return void
     */
    public function updateCamper($camper) {
        $status = DB::table('campers')
            ->where('id', $camper->id)
            ->update([
                'first_name' => $camper->first_name,
                'last_name' => $camper->last_name,
                'gender' => $camper->gender,
                'date_of_birth' => $camper->date_of_birth,
                'updated_at' => Carbon::now()
            ]);
    }

    public function updateRegistrationInfo($camper) {
        $status = DB::table('registrations')
            ->where('id', $camper->id)
            ->update([
                'first_name' => $camper->first_name,
                'last_name' => $camper->last_name,
                'gender' => $camper->gender,
                'date_of_birth' => $camper->date_of_birth,

                'updated_at' => Carbon::now()
            ]);
    }

    /**
     * Get camper by ID
     * 
     * @param integer $camper
     * @return Illuminate\Support\Collection;
     */
    public function getCamper($camper) {
        $camper = DB::table('campers')->where('id', $camper)->first();
        return $camper;
    }

    /**
     * Save registration form
     * 
     * @param array $registrationForm
     */
    public function saveRegistration($registrationForm) {
        $status = DB::table('registration')->insert($registrationForm);

        // TODO
    }
}
