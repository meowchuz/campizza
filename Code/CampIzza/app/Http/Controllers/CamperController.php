<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use Carbon\Carbon;

use App\Facades\Camper;
use App\Facades\Time;
use App\Repositories\CamperRepository;

class CamperController extends Controller
{
    /**
     * The CamperRepository instance.
     *
     * @var App\Repositories\CamperRepository
     */
    protected $camperRepository;

    /**
     * Create a new CamperController instance.
     *
     * @param  App\Repositories\CamperRepository $camperRepository
     * @return void
    */
    public function __construct(CamperRepository $camperRepository) {
        $this->camperRepository = $camperRepository;
    }

    /**
     * Show campers
     * 
     * @param  Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function campers(Request $request) {
        $request->user()->authorizeRoles(['parent', 'owner']);

        if ('owner' == Auth::user()->roles[0]->name) {
            $campers = $this->camperRepository->getCampersWithPagination();

            $totalItemsCount = $campers->total();
            $numberOfItemsPerPage = $campers->perPage();
            $page = $campers->currentPage();

            $numberOfPages = floor(($totalItemsCount + $numberOfItemsPerPage - 1) / $numberOfItemsPerPage);
            $start = ($page * $numberOfItemsPerPage) - ($numberOfItemsPerPage - 1);
            $end = min($start + $numberOfItemsPerPage - 1, $totalItemsCount);

            if (0 == $campers->total()) {
                $start = 0;
            }

            return view('pages.owner.campers', [
                'start' => $start,
                'end' => $end,
                'campers' => $campers,
                'phoneTypes' => Camper::getPhoneTypes()
            ]);
        }

        $campers = $this->camperRepository->getCampers(Auth::user()->id);

        $currenYear = date('Y');
        
        return view('pages.parent.campers', [
            'months' => Time::getMonths(),
            'currenYear' => $currenYear,
            'campers' => $campers
        ]);
    }

    /**
     * Store camper information
     * 
     * @param  Illuminate\Http\Request $request
     * @return Illuminate\Http\RedirectResponse
     */
    public function addCamper(Request $request) {
        try {
            DB::beginTransaction();

            $camper = (object) [
                'parent' => Auth::user()->id
            ];
            $attributes = Camper::getCamperAttributes('profile');

            foreach ($attributes as $attribute) {
                $camper->{$attribute} = $request->input($attribute);
            }

            $this->camperRepository->addCamper($camper);

            $this->camperRepository->storeCamperInfo($camper);

            DB::commit();

            return redirect('campers', 201);
        } catch (Exception $e) {
            DB::rollback();
        }
    }

    /**
     * Update camper information
     * 
     * @param  Illuminate\Http\Request $request
     * @param integer $camper
     * @return Illuminate\Http\RedirectResponse
     */
    public function updateCamper(Request $request, $camper) {
        try {
            DB::beginTransaction();

            $camper = (object) [
                'id' => $camper
            ];
            $attributes = Camper::getCamperAttributes('profile');

            foreach ($attributes as $attribute) {
                $camper->{$attribute} = $request->input($attribute);
            }

            $this->camperRepository->updateCamper($camper);
            $this->camperRepository->updateRegistrationInfo($camper);

            DB::commit();

            return redirect('campers', [
                'camper' => $camper]);
        } catch (Exception $e) {
            DB::rollback();
        }
    }

    /**
     * Show registration
     * 
     * @return \Illuminate\Http\Response
     */
    public function registerCamp() {
        $campers = $this->camperRepository->getCampers(Auth::user()->id);

        $currenYear = date('Y');
        
        return view('pages.parent.registration', [
            'months' => Time::getMonths(),
            'currenYear' => $currenYear,
            'campers' => $campers
        ]);
    }

    /**
     * Show registration camp
     * Step 1: Information and Uniform
     * 
     * @param  Illuminate\Http\Request $request
     * @param integer $camper
     * @return Illuminate\Http\RedirectResponse
     */
    public function registerCampAboutInformation(Request $request, $camper) {
        $camper = $this->camperRepository->getCamper($camper);

        return view('pages.parent.registration_information', [
            'camper' => $camper,
            'tShirtSizes' => Camper::getTShirtSizes()
        ]);
    }

    /**
     * Handle registration camp
     * Step 1: Information and Uniform
     * 
     * @param  Illuminate\Http\Request $request
     * @param integer $camper
     * @return Illuminate\Http\RedirectResponse
     */
    public function handleRegisterCampAboutInformation(Request $request, $camper) {
        $attributes = Camper::getCamperAttributes('information');

        foreach ($attributes as $attribute) {
            $request->session()->put($attribute, $request->input($attribute));
        }
        try{
                DB::beginTransaction();
                
                DB::table('registrations')
                ->where([
                    ['id', $camper],
                    ['user', Auth::user()->id],
                ])
                ->update([
                    'current_school' => $request->session()->get('current_school'),
                    'grade_in_the_fall' => $request->session()->get('grade_in_the_fall'),
                    'tshirt_size' => $request->session()->get('tshirt_size'),
                    'number_of_tshirt' => $request->session()->get('number_of_tshirt'),

                    'created_at' =>  Carbon::now(),
                    'updated_at' => Carbon::now()
                    ]);
        
                DB::commit();
    
                return redirect()->route('registerCampAboutHealth', [
                'camper' => $camper
                ]);
        } catch (Exception $e) {
            DB::rollBack();
        }

    }

    /**
     * Show registration camp
     * Step 2: Health
     * 
     * @param  Illuminate\Http\Request $request
     * @param integer $camper
     * @return Illuminate\Http\RedirectResponse
     */
    public function registerCampAboutHealth(Request $request, $camper) {
        $camper = $this->camperRepository->getCamper($camper);

        return view('pages.parent.registration_health', [
            'camper' => $camper
        ]);
    }

    /**
     * Handle registration camp
     * Step 2: Health
     * 
     * @param  Illuminate\Http\Request $request
     * @param integer $camper
     * @return Illuminate\Http\RedirectResponse
     */
    public function handleRegisterCampAboutHealth(Request $request, $camper) {
        $attributes = Camper::getCamperAttributes('health');

        foreach ($attributes as $attribute) {
            $request->session()->put($attribute, $request->input($attribute));
        }
        try{
            DB::beginTransaction();
            
            foreach ($attributes as $attribute) {
                DB::table('registrations')
                ->where([
                    ['id', $camper],
                    ['user', Auth::user()->id],
                ])
                ->update([
                    $attribute => $request->session()->get($attribute)
                    ]);
            }
            DB::commit();
            return redirect()->route('registerCampAboutSession', [
                'camper' => $camper
            ]);
    } catch (Exception $e) {
        DB::rollBack();
    }
    }

    /**
     * Show registration camp
     * Step 3: Session
     * 
     * @param  Illuminate\Http\Request $request
     * @param integer $camper
     * @return Illuminate\Http\RedirectResponse
     */
    public function registerCampAboutSession(Request $request, $camper) {
        $camper = $this->camperRepository->getCamper($camper);

        $mockSchedule = [
            'schedule' => [
                'week_1' => 'June 25 - June 29',
                'week_2' => 'July 2 - July 6',
                'week_3' => 'July 9 - July 13',
                'week_4' => 'July 16 - July 20',
                'week_5' => 'July 23 - July 27'
            ],
            'notice' => [
                'No camp in July 4th'
            ]
        ];

        return view('pages.parent.registration_session', [
            'camper' => $camper,
            'schedule' => $mockSchedule
        ]);
    }

    /**
     * Handle registration camp
     * Step 3: Session
     * 
     * @param  Illuminate\Http\Request $request
     * @param integer $camper
     * @return Illuminate\Http\RedirectResponse
     */
    public function handleRegisterCampAboutSession(Request $request, $camper) {
        $attributes = Camper::getCamperAttributes('session');

        foreach ($attributes as $attribute) {
            $request->session()->put($attribute, $request->input($attribute));
        }

        for ($index = 1; $index < 6; $index++) {
            if ('week_' . $index . '_BOTH' == $request->session()->get('week_' . $index)) {
                $request->session()->put('Week' . $index . 'AM', 1);
                $request->session()->put('Week' . $index . 'PM', 1);
            } elseif ('week_' . $index . '_AM' == $request->session()->get('week_' . $index)) {
                $request->session()->put('Week' . $index . 'AM', 1);
                $request->session()->put('Week' . $index . 'PM', 0);
            } elseif ('week_' . $index . '_PM' == $request->session()->get('week_' . $index)) {
                $request->session()->put('Week' . $index . 'AM', 0);
                $request->session()->put('Week' . $index . 'PM', 1);
            } else {
                $request->session()->put('Week' . $index . 'AM', 0);
                $request->session()->put('Week' . $index . 'PM', 0);
            }
        }

        try{
            DB::beginTransaction();
            
            foreach ($attributes as $attribute) {
                
                if ($attribute != 'week_1' && $attribute != 'week_2' && $attribute != 'week_3' && $attribute != 'week_4' && $attribute != 'week_5' && $attribute != 'week_6') 
                {
                    DB::table('registrations')
                    ->where([
                        ['id', $camper],
                        ['user', Auth::user()->id],
                    ])
                    ->update([
                        $attribute => $request->session()->get($attribute)
                    ]);
                }
            } 
                DB::commit();
        
                return redirect()->route('registerCampAboutReview', [
                    'camper' => $camper
                ]);
    } catch (Exception $e) {
        DB::rollBack();
    }
    }

    /**
     * Show registration camp
     * Step 4: Review
     * 
     * @param  Illuminate\Http\Request $request
     * @param integer $camper
     * @return Illuminate\Http\RedirectResponse
     */
    public function registerCampAboutReview(Request $request, $camper) {
        $camper = $this->camperRepository->getCamper($camper);
        $registrationForm = (object) $request->session()->all();

        return view('pages.parent.registration_review', [
            'camper' => $camper,
            'registrationForm' => $registrationForm
        ]);
    }

    /**
     * Handle registration camp
     * Step 4: Review
     * 
     * @param  Illuminate\Http\Request $request
     * @param integer $camper
     * @return Illuminate\Http\RedirectResponse
     */
    public function handleRegisterCampAboutReview(Request $request, $camper) {
        $attributes = Camper::getCamperAttributes();
        $registrationInformation = [];

        foreach ($attributes as $attribute) {
            array_push($registrationInformation, $request->session()->get($attribute));
        }

        // TODO
        echo json_encode($registrationInformation);
        die();
    }
}
