<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use DB;

use App\Http\Requests;

class reportController extends Controller
{
    /**
     * Get registration data from camper for Extended Care report
     * 
     * @return Illuminate\Support\Collection;
     */
    public function extendedCareReport(Request $request) {

        $request->user()->authorizeRoles(['owner']);
        if ($request->input('extended-sort') == 'new'){
            $extendedData = DB::table('registrations')
                            ->orderBy('created_at', 'asc')
                            ->get();
        }
        else {
            $extendedData = DB::table('registrations')
                            ->orderBy('first_name', 'asc')
                            ->paginate(2);
        }
        
        return view ('pages.owner.extendedCare-report')->with('extendedData', $extendedData);                

    }

    /**
     * Get registration data from camper for Allergies report
     * 
     * @return Illuminate\Support\Collection;
     */
    public function allergiesReport(Request $request) {
        $request->user()->authorizeRoles(['owner']);
        $extendedData = DB::table('registrations')
                        ->orderBy('created_at', 'desc')
                        ->get();
        return view ('pages.owner.allergies-report')->with('extendedData', $extendedData); 
    }

    /**
     * Get registration data from camper for tShirt report
     * 
     * @return Illuminate\Support\Collection;
     */
    public function tShirtReport(Request $request) {
        $request->user()->authorizeRoles(['owner']);
        $extendedData = DB::table('registrations')
                        ->orderBy('created_at', 'desc')
                        ->get();
        return view ('pages.owner.tShirt-report')->with('extendedData', $extendedData);  
        
    }

}