<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\Facades\Schedule;
use App\Repositories\ScheduleRepository;

class ScheduleController extends Controller
{
    /**
     * The ScheduleRepository instance.
     *
     * @var App\Repositories\ScheduleRepository
     */
    protected $scheduleRepository;

    /**
     * Create a new ScheduleController instance.
     *
     * @param  App\Repositories\ScheduleRepository $scheduleRepository
     * @return void
    */
    public function __construct(ScheduleRepository $scheduleRepository) {
        $this->scheduleRepository = $scheduleRepository;
    }

    /**
     * Show list of schedules
     * 
     * @return \Illuminate\Http\Response
     */
    public function schedules() {
        $schedules = $this->scheduleRepository->getSchedulesWithPagination();

        $totalItemsCount = $schedules->total();
        $numberOfItemsPerPage = $schedules->perPage();
        $page = $schedules->currentPage();

        $numberOfPages = floor(($totalItemsCount + $numberOfItemsPerPage - 1) / $numberOfItemsPerPage);
        $start = ($page * $numberOfItemsPerPage) - ($numberOfItemsPerPage - 1);
        $end = min($start + $numberOfItemsPerPage - 1, $totalItemsCount);

        if (0 == $schedules->total()) {
            $start = 0;
        }

        return view('pages.owner.schedules', [
            'start' => $start,
            'end' => $end,
            'schedules' => $schedules
        ]);
    }

    /**
     * Create new schedule
     * 
     * @param  Illuminate\Http\Request $request
     * @return Illuminate\Http\RedirectResponse
     */
    public function addSchedule(Request $request) {
        try {
            DB::beginTransaction();

            $schedule = (object) [];
            $attributes = Schedule::getScheduleAttributes();

            foreach ($attributes as $attribute) {
                $schedule->{$attribute} = $request->input($attribute);
            }

            $monday = $schedule->start_date;
            for ($index = 1; $index < 6; $index++) {
                $monday = date('F d', strtotime('monday this week', strtotime($monday)));
                $friday = date('F d', strtotime('friday this week', strtotime($monday)));

                $schedule->{'week_' . $index} = $monday . ' - ' . $friday;
                $monday = date('F d', strtotime('next monday', strtotime($monday)));
            }

            $this->scheduleRepository->saveSchedule($schedule);

            DB::commit();

            return redirect('schedules', 201);
        } catch (Exception $e) {
            DB::rollback();
        }
    }
}
