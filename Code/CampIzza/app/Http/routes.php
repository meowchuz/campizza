<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/**
 * Authentication
 */
Route::auth();

Route::get('/changePassword','HomeController@showChangePasswordForm');
Route::post('/changePassword','HomeController@changePassword')->name('changePassword');

/**
 * Dashboard
 */
Route::group(['middleware' => ['auth', 'web']], function() {

    Route::get('/contact', 'ParentController@contact')->name('contact');
    Route::post('/contact', 'ParentController@saveContact')->name('saveContact');
    Route::patch('/contact', 'ParentController@updateContact')->name('updateContact');

    Route::get('/parents', 'ParentController@parents')->name('parents');

    Route::get('/campers', 'CamperController@campers')->name('campers');
    Route::post('/campers', 'CamperController@addCamper')->name('addCamper');
    Route::patch('/campers/{camper}', 'CamperController@updateCamper')->name('updateCamper');

    Route::get('/registration', 'CamperController@registerCamp')->name('registerCamp');
    Route::get('/registration/{camper}/information', 'CamperController@registerCampAboutInformation')->name('registerCampAboutInformation');
    Route::post('/registration/{camper}/information', 'CamperController@handleRegisterCampAboutInformation')->name('handleRegisterCampAboutInformation');
    Route::get('/registration/{camper}/health', 'CamperController@registerCampAboutHealth')->name('registerCampAboutHealth');
    Route::post('/registration/{camper}/health', 'CamperController@handleRegisterCampAboutHealth')->name('handleRegisterCampAboutHealth');
    Route::get('/registration/{camper}/session', 'CamperController@registerCampAboutSession')->name('registerCampAboutSession');
    Route::post('/registration/{camper}/session', 'CamperController@handleRegisterCampAboutSession')->name('handleRegisterCampAboutSession');
    Route::get('/registration/{camper}/review', 'CamperController@registerCampAboutReview')->name('registerCampAboutReview');
    Route::post('/registration/{camper}/review', 'CamperController@handleRegisterCampAboutReview')->name('handleRegisterCampAboutReview');

    Route::get('/schedules', 'ScheduleController@schedules')->name('schedules');
    Route::post('/schedules', 'ScheduleController@addSchedule')->name('addSchedule');

    // *****************************************************************************************************************
    Route::get('extendedCare-report','reportController@extendedCareReport')->name('extendedCare-report');
    Route::get('allergies-report','reportController@allergiesReport')->name('allergies-report');
    Route::get('tShirt-report','reportController@tShirtReport')->name('tShirt-report');
    // *****************************************************************************************************************

    Route::group(['middleware' => ['user']], function() {
        Route::get('/', 'HomeController@index');
    });
});

/**
 * APIs
 */
Route::group(['prefix' => 'api'], function() {
    Route::get('/countries', 'ApiController@getCountries');
    Route::get('/states', 'ApiController@getStates');
    Route::get('/states/{state}', 'ApiController@getCitiesByState');

    Route::get('/recaptcha', 'ApiController@verifyReCaptcha');

    Route::get('/campers/{camper}', 'ApiController@camper');
    Route::get('/parents/{parent}', 'ApiController@parent');
});
