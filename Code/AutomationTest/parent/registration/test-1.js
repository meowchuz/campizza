var webdriverio = require('webdriverio');

var options = {
  desiredCapabilities: {
    browserName: 'chrome'
  }
};

webdriverio
  .remote(options)
  .init()
  .url('http://localhost:8000/login')

  .setValue('#input-email', 'vhnam2504@gmail.com')
  .setValue('#input-password', '123123')
  .click('#btn-submit')

  .click('#navigation-registration')

  .click('#camper-1')

  .setValue('#input-current-school', 'Cypress Elementary')
  .setValue('#input-grade-in-the-fall', '4')
  .selectByValue('#input-tshirt-size', 'L')
  .setValue('#input-number-of-tshirt', '1')
  .click('#btn-submit')

  .setValue('#input-doctor-name', 'Dr Shahla Heshmati')
  .setValue('#input-insurance', 'Blue Shield')
  .setValue('#input-doctor-phone', '9497261100')
  .setValue('#input-policy-holder', 'Sahar Mahmoud')
  .setValue('#input-illnesses', 'None')
  .setValue('#input-allergies', 'None')
  .selectByValue('#input-medication', 'No')
  // .setValue('#input-medication-name', '')
  .selectByValue('#input-activities', 'No')
  // .setValue('#input-activities-name', '')
  .selectByValue('#input-medical-treatments', 'No')
  // .setValue('#input-medical-treatments-name', '')
  .selectByValue('#input-immunizations', 'No')
  .setValue('#input-tetanus', '08/03/2018')
  .setValue('#input-comments', 'None')
  .click('#btn-submit')

  // .click('#week_1_BOTH')
  // .click('#week_2_BOTH')
  // .click('#week_3_BOTH')
  .click('#week_4_BOTH')
  .click('#week_5_BOTH')
  // .click('#extended-care')
  .click('#btn-submit')

  // .end()

  .catch(function(err) {
    console.error(err);
  });
