# Excutive Summary
The critical document for new and growing businesses for CampIzza

## Summary
### Purpose
A registration system for parents to register their children in the summer program and a way in which those registration data to be incorporated into a roster for digital/online attendance taking. All the data should accessible and easy to analyze for statistics, trends and program development. The ideal solution is one that is easy to modify each summer for the varying dates, number of weeks, birth years, etc.
### Scope
- Timeline: 10 weeks
- Budget:
    - SECRET
    - SECRET
    - SECRET
### Main features
The main concern is making sure the database/registration can be modified to address annual adjustments to the program length. The system also needs to assist in:
1. collecting registration
2. generating rosters
3. taking attendance
4. analyzing attendance/registration data
5. billing
6. automation

## Customer
- Organization: Camp Izza
- Website: www.campizza.com
- Description: Camp Izza is a summer day camp where campers have fun and develop pride in being who they are.

## Difficulties and Needs
### Difficulties
Currently the summer camp has a operable website where users can pay and register. However, the current system has difficulties in recording that data and also visibility issues for the registration process. The current automated data entry can be so problematic that Omar, the camp owner, has to manually go in at night and fix his data points on excel. Additionally, users have a hard time adjusting their schedule after selecting if they want to be there for a full-day or half-day.
### Needs
- Methods are easily update registration (DOB, dates of camp, etc)
- Emergency Form Generator
- Lists (shirts, allergies, extended care)
- Data analysis (registration, attendance)

## Risks and Opportunity
### Risks
- Requirements misconceptions
- Lack of linkage between team members and customer
- No experience with reality project
- Scalability, evolution of project when customer changes requirements
- Missing deadline
- Expectation of operating, maintenance and upgrading costs
### Opportunity
- Have experience on reality project, such as business analysis, design, management,...
