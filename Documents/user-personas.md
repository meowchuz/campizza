# User personas
Personas are fictional characters designers use to reflect user types, pinpointing who they are and what they do with products in relevant contexts. Designers create personas from user data, to understand user characteristics, needs, goals, etc. and gain valuable insights into user journeys, and later, test prototypes.

## Characters
- Camp owner
- Couselor
- Children
- Parents

## User analysis
### Camp owner
- Name: Ozzma
- Goals/Characteristics:
    - To be able to access and create the rosters
    - Access the attendance of each class 
    - Access the oops reports
- Scenario:
    - Before the camp begins every summer, the camp owner is able to access the automatically updated roster from the website. From the roster he should be able to create the classrooms based on the hours registered and the grade of the children.
    - After the camp begins the camp owner should be able to access the attendance of each classroom from the counselors. He should also be able to access if any oops reports were created and if they were before the end of the day make sure parents are aware of such accidents.
### Counselor
- Name: David
- Goals/Characteristics:
    - Taking attendance kids on Summer camp every day.
    - Review and report camper attendance easily at the end of the day.
    - File a report if any kid gets hurt to their parent.  
- Scenario:
    - David is taking attendance of 10 new kids who just attended the summer camp today. He wants to know how many kids often go to the camp so he can assign new kids to properly group. Unfortunately, the data she has on her Kindle tablet is unorganized and impossible to check the report. While he is thinking about the solution, one of ten new kids in the camp get hurt when he was trying to get some water from a hot water pot. The system doesn’t have feature for his to fill the accident form and send it to kid’s parent. Jennie doesn’t know how to deal with it, he has to call her manager to get the instruction. If the system has filling accident form feature. It can help his fill the accident form quickly and send automatically to the kid’s parent.
### Child
- Name: Christina
- Goals/Characteristics:
    - Join camp activities
    - Register for summer camp.
    - Buy Camp Izza printed t-shirt.
- Scenario: 
    - Christina saw a Camp Izza flyer in his elementary school. He came back home with the flyer and told her parents that he wants to join the summer camp.
    - Her parents then registered the camp online. Upon arrival, Christina gets a camp t-shirt and finds a lot of his classmates there. They learn cool things together and play soccer in their spare time. Christina is having a good time there.
    - When summer ends, Christina wants to have another camp t-shirt but she cannot order more! The system doesn’t allow buying more than one shirts.
### Parents
- Name: Fred
- Goals/Characteristics:
    - Fred is a single father who raises his three kids.
    - He works from 8:00 pm - 4:00 pm every day.
    - He would like a system where he can easily register his children and upload medical information or submit information if something were to change. 
- Scenario:
    - Fred realizes that summer is quickly approaching and needs to make summer plans for his children. One day while on his computer looking up summer camps he comes across Camp Izza. He looks through the website and sees it is nearby and offers a and a full-day option. He is hesitant as he is afraid the registration process can be confusing but he decides to fill it out and finds it surprisingly easy and quick. He notices that after registering his children through his account he can update their information. Fred loves Camp Izza and can not wait for his children to attend.
